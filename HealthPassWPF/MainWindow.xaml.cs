﻿using HealthPassWPF.ViewModels;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;


namespace HealthPassWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly HealthPassViewModel _healthPassViewModel;

        public MainWindow()
        {
            InitializeComponent();
            _healthPassViewModel = new HealthPassViewModel();
            this.DataContext = _healthPassViewModel;
        }

        private void GenerateUsers(object sender, RoutedEventArgs e)
        {
            this._healthPassViewModel.GenerateUsers();
        }

        private void GenerateHealthPass(object sender, RoutedEventArgs e)
        {
            GenTextBox.Visibility = Visibility.Visible;
            this._healthPassViewModel.GenerateHealthPasses();
        }
    }
}
