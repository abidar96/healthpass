﻿using ConcurrentPriorityQueue.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthPassWPF.Models;

public class User : IHavePriority<int>
{
    public string Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public DateTime Birthday { get; set; }

    public bool CovidState { get; set; }

    public DateTime RequestDate { get; set; }

    public DateTime GeneratingDate { get; set; }

    public int Priority { 
        get => CovidState ? 0 : 1;
        set => Priority = value; 
    }
}
