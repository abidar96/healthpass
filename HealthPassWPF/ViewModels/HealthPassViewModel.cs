﻿using HealthPassWPF.Models;
using HealthPassWPF.Services;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace HealthPassWPF.ViewModels;

public class HealthPassViewModel : INotifyPropertyChanged
{

    public event PropertyChangedEventHandler PropertyChanged;


    private List<User> _users;

    public List<User> Users
    {
        get
        {
            return _users;
        }
        set
        {
            _users = value;
            OnPropertyRaised(nameof(Users));
        }
    }

    private readonly UserService _userService;


    private int _counter;
    public int Counter {
        get
        {
            return _counter;
        }
        set
        {
            _counter = value;
            OnPropertyRaised(nameof(Counter));
        }
    }

    private string _genTextBlock ;
    public string GenTextBlock
    {
        get
        {
            return _genTextBlock;
        }
        set
        {
            _genTextBlock = value;
            OnPropertyRaised(nameof(GenTextBlock));
        }
    }



    public HealthPassViewModel()
    {
        Counter = 0;
        GenTextBlock = $"Géneration En cours: {this.Counter}%";
        Users = new List<User>();
        PropertyChangedEventHandler prop = (s, e) =>
        {
            if(s is UserService ifout)
            {
                Counter = ifout.counter;
                if (Counter == 100)
                    GenTextBlock = $"Géneration Terminée: {this.Counter}%";
                else
                    GenTextBlock = $"Géneration En cours: {this.Counter}%";
                Debug.WriteLine($"counter : {Counter}");
            }
            
        };
        _userService = new UserService(prop);
    }


    public void GenerateUsers()
    {
        Users = _userService.GenerateUsers();
    }

    public void GenerateHealthPasses()
    {
        _userService.GenerateAllHealthPassAsync();
    }

    private void OnPropertyRaised(string propertyname)
    {
        if (PropertyChanged != null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
        }
    }


}
