﻿using ConcurrentPriorityQueue.Core;
using HealthPassWPF.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace HealthPassWPF.Services;

public class UserService : INotifyPropertyChanged
{
    public ConcurrentDictionary<string, User> users ;
    private readonly ConcurrentPriorityQueue<IHavePriority<int>, int> priorityQueue;
    readonly object obj = new object();

    private readonly Random _random = new Random();
    public int counter { get; set;}
    public event PropertyChangedEventHandler PropertyChanged;
    private readonly string _path;
    public string Path
    {
        get { return _path; }
    }

    public UserService(PropertyChangedEventHandler propertyChangedEventHandler)
    {
        _path = $"{Directory.GetCurrentDirectory()}\\Passes";
        counter = 0;
        users = new ConcurrentDictionary<string, User>();
        priorityQueue = new ConcurrentPriorityQueue<IHavePriority<int>, int>();
        PropertyChanged = propertyChangedEventHandler;
    }

    protected void OnPropertyChanged([CallerMemberName] string name = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }

    public List<User> GenerateUsers()
    {
        var users = new List<User>();

        Parallel.For(0, 100, i => {
            var user = new User()
            {
                Id = Guid.NewGuid().ToString("N"),
                Birthday = DateTime.Now,
                CovidState = _random.NextDouble() > 0.5,
                FirstName = generateRandomString(),
                LastName = generateRandomString(),
                RequestDate = DateTime.Now,
            };
            this.users[user.Id] = user;
        });
        foreach(var x in this.users)
        {
            users.Add(x.Value);
        }

        return users;
    }

    public async Task GenerateAllHealthPassAsync()
    {
        Parallel.ForEach(users, user =>
        {
            priorityQueue.Enqueue(user.Value);
        });

        var buffer = new BufferBlock<User>();
        var produceTask = ProduceHealthPass(buffer);
        ConsumeHealthPassForUser(buffer);
        await produceTask;
    }

    private Task ProduceHealthPass(ITargetBlock<User> target)
    {
        Task producer = Task.Factory.StartNew(() => {
            Parallel.ForEach(priorityQueue, new ParallelOptions
            {
                MaxDegreeOfParallelism = 10
            },
            data =>{
                var user = priorityQueue.Dequeue().Value as User;
                _ = target.Post(user);
            });
            target.Complete();
        });
        return producer;
    }

    private void ConsumeHealthPassForUser(ISourceBlock<User> source)
    {
        // Limit Consumers on 10;
        // For no limited threads, we can use while (await source.OutputAvailableAsync()) instead of for loop, that will let threadPool manage the number of threads
        Task[] Consumers = new Task[10];


        for (int i = 0; i < 10; i++)
        {
            Consumers[i] = Task.Run(async () =>
            {
                while (await source.OutputAvailableAsync())
                {
                    User user = await source.ReceiveAsync();
                    user.GeneratingDate = DateTime.Now;
                    Directory.CreateDirectory(_path);
                    using (FileStream fs = File.Create($"{_path}\\{user.Id}.txt"))
                    {
                        byte[] info = new UTF8Encoding(true).GetBytes(user.FirstName + " " + user.LastName + " " + user.CovidState + " " + DateTime.Now);
                        // Add user information to the file.
                        fs.Write(info, 0, info.Length);
                    }

                    lock (obj)
                    {
                        counter++;
                        OnPropertyChanged();
                    }
                }
            });
        }

        Task.WaitAll(Consumers);
    }

    private string generateRandomString()
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        return new string(Enumerable.Repeat(chars, 6)
            .Select(s => s[_random.Next(s.Length)]).ToArray());

    }
}
