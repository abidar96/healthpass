using HealthPassWPF.Models;
using HealthPassWPF.Services;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;

namespace UnitTest
{
    public class UserServiceTest
    {
        [Test]
        public void GenerateUsersShouldReturnListOfUsers()
        {
            UserService _userService = new UserService(null);
            //ACT
            List<User> users = _userService.GenerateUsers();
            
            //Assert
            Assert.AreEqual(100, users.Count);
            foreach(var user in users)
                Assert.IsNotNull(user);
        }

        [Test]
        public void GenerateAllHealthPassShouldCreateFiles()
        {
            //Arrange
            UserService _userService = new UserService(null);
            _userService.GenerateUsers();


            //ACT
            _ = _userService.GenerateAllHealthPassAsync();
            DirectoryInfo dir = new DirectoryInfo(_userService.Path);

            //Assert
            Assert.AreEqual(100, dir.GetFiles().Length);

            //Clean
            Directory.Delete(_userService.Path, true);
        }

    }
}