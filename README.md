# Health pass Generator


Ce projet est pour le katas C# Générateur de passes sanitaire de Exalt-It, 

### Le fonctionnelle
L'application est une application WPF (Desktop), l'application génére aléatoirement 100 utilisateur positif ou négatif, puis  elle génère en parallèle les passes sanitaires en priorisant les utilisateurs positifs. L'interface UI affiche également l'état d''avancement de la génération des passes vie une progresse bar.

### Le projet :

Le projet est une application WPF en .Net Core 6.0 .

L'objectif du Katas est de faire la programmation multithreading en C#.

Le projet est basé sur l'architecture MVVM.

Le projet utilise le design pattern Producer / Consumer.

La priorisation de la génération des passes sanitaire positifs est gérées par priorityQueue.

Le projet contient également les tests unitaires.
